ARG APACHE_VERSION=ND
FROM registry.gitlab.com/jhmdocker/image/apache:${APACHE_VERSION}

ENV CONFIG_FILE=/etc/webdav/config.yaml

ENV APACHE_MODS="auth_basic"

RUN apt-get update && \
    apt-get install -y python3-pip python3-setuptools python3-wheel jq && \
    pip3 install --break-system-packages yq

RUN a2enmod dav && \
    a2enmod dav_fs && \
    a2dismod mpm_prefork && \
    a2enmod mpm_event

RUN mkdir -p /var/webdav && \
    chown www-data.www-data /var/webdav && \
    mkdir -p /etc/webdav

COPY rootfs /

ENTRYPOINT [ "/usr/local/bin/webdav-entrypoint.sh" ]
