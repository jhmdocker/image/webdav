APACHE_VERSION=2.4
VERSION=latest
IMG=registry.gitlab.com/jhmdocker/image/webdav

build:
	docker build $(BUILD_OPTS) -t $(IMG) \
		--build-arg APACHE_VERSION=$(APACHE_VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(VERSION)
	docker push $(IMG)
	docker push $(IMG):$(VERSION)
