#!/bin/bash -e

# query the config file for a value
function get_config() {
  local QUERY="$1"
  local DEFAULT="$2"

  V="$(echo "$CONFIG_FILE_JSON" | jq -r "$QUERY")"
  if [[ "${V}" == "null" || "${V}" == "" ]]; then 
    V="$DEFAULT"
  fi
  printf "%s" "$V"
}

# echo to stderr
function logerr() {
  >&2 echo "$1"
}

# appends users from the Yaml config into the a given password file
# does not delete existing usernames, but replaces to avoid duplicates
function add_users_to_file() {
  local SHARE_NAME="$1"
  local JSON="$2"
  local FILENAME="$3"
  local LENGTH="$(echo "$JSON" | jq -r ". | length" )"
  local x=0
  while [[ $x -lt $LENGTH ]]; do
    local USER_NAME="$(         echo $JSON | jq -r ".[$x].name")"
    local USER_PASSWORD="$(     echo $JSON | jq -r ".[$x].password")"
    local USER_PASSWORDHASH="$( echo $JSON | jq -r ".[$x].\"password-hash\"")"

    # sanity checks for users
    if [[ "${USER_NAME}" == "" ]]; then
      echo "ERROR: [share: $SHARE_NAME] empty username at position $x"
      continue
    fi

    # determine password hash
    hash=
    if [[ "${USER_PASSWORD}" != "" ]]; then
      hash+=$(/usr/bin/htpasswd -nbB "$USER_NAME" "$USER_PASSWORD")
    elif [[ "${USER_PASSWORDHASH}" != "" ]]; then
      hash+="${USER_NAME}:$USER_PASSWORDHASH"
    fi

    # remove existing user
    sed -i "s/${USER_NAME}\:.*//g" "${FILENAME}"
    
    # remove empty lines
    sed -i '/^\s*$/d' "${FILENAME}"
    
    # append user
    echo "$hash" >> "${FILENAME}"
    
    let x+=1

  done
}

# config filename sanity check
if [[ ! -f "${CONFIG_FILE}" ]]; then 
  logerr "ERROR: File does not exist ${CONFIG_FILE}"
  exit 1
fi

# convert yaml config to json
CONFIG_FILE_JSON=$(yq '.' "$CONFIG_FILE")

# count how many share entries are in the config
SHARELISTLEN=$(get_config '.shares | length')

# global htpasswd file for all shares
USERS_PASSWORD_FILE=$(get_config '."password-file"')
if [[ "${USERS_PASSWORD_FILE}" == "" ]]; then 
  USERS_PASSWORD_FILE="/etc/webdav/_global.users.htpasswd"
fi

x=0
while [[ $x -lt $SHARELISTLEN ]]; do

  SHARE_NAME="$(                         get_config ".shares[$x].name"                                   )"
  SHARE_AUTHNAME="$(                     get_config ".shares[$x].authname"                               )"
  SHARE_CONTEXTROOT="$(                  get_config ".shares[$x].contextroot"                            )"
  SHARE_DIRECTORY_PATH="$(               get_config ".shares[$x].directory.path"                         )"
  SHARE_DIRECTORY_DIRECTIVES="$(         get_config ".shares[$x].directory.directives"                   )"
  SHARE_DIRECTORY_REPLACE_DIRECTIVES="$( get_config ".shares[$x].directory.\"replace-directives\"" false )"
  SHARE_PASSWORD_FILE="$(                get_config ".shares[$x].\"password-file\""                      )"

  # sanity checks for the share
  if [[ "${SHARE_NAME}" == "" ]]; then
    logerr "WARNING: Ignoring share at position $x. No name informed."
    let x+=1
    continue
  fi

  if [[ "${SHARE_DIRECTORY_PATH}" == "" ]]; then
    SHARE_DIRECTORY_PATH="/var/webdav/${SHARE_NAME}"
  fi
  if [ ! -d "${SHARE_DIRECTORY_PATH}" ]; then
    logerr "WARNING: [$SHARE_NAME] Ignoring. Directory does not exist $SHARE_DIRECTORY_PATH"
    let x+=1
    continue
  fi
  if [[ "${SHARE_AUTHNAME}" == "" ]]; then
    SHARE_AUTHNAME="$SHARE_NAME"
  fi
  if [[ "${SHARE_CONTEXTROOT}" == "" ]]; then
    SHARE_CONTEXTROOT="$SHARE_NAME"
  fi
  if [[ "${SHARE_PASSWORD_FILE}" == "" ]]; then
    SHARE_PASSWORD_FILE="/etc/webdav/${SHARE_NAME}.htpasswd"
  fi

  touch "${SHARE_PASSWORD_FILE}" "${SHARE_PASSWORD_FILE}.gen"
  if [[ -f "${SHARE_PASSWORD_FILE}" ]]; then
    cp "${SHARE_PASSWORD_FILE}" "${SHARE_PASSWORD_FILE}.gen"
  fi

  # if global htpasswd file already exists, then append
  if [[ -f "${USERS_PASSWORD_FILE}" ]]; then
    cat "${USERS_PASSWORD_FILE}" > "${SHARE_PASSWORD_FILE}.gen"
  fi
  # append other users defined in Yaml config
  add_users_to_file "$SHARE_NAME" "$(get_config ".users"           )" "${SHARE_PASSWORD_FILE}.gen"
  add_users_to_file "$SHARE_NAME" "$(get_config ".shares[$x].users")" "${SHARE_PASSWORD_FILE}.gen"

  # count non empty lines
  # number of users already included in the given password file
  SHARE_PASSWORD_FILE_LINECOUNT=$(cat "${SHARE_PASSWORD_FILE}.gen" | sed '/^\s*$/d' | wc -l)

  logerr "WARNING: Total users found $SHARE_PASSWORD_FILE_LINECOUNT"

  echo "  #######################################"
  echo "  ## Share: $SHARE_NAME"
  echo "  #######################################"

  echo "  Alias /${SHARE_CONTEXTROOT} ${SHARE_DIRECTORY_PATH}"
  echo "  <Directory ${SHARE_DIRECTORY_PATH}>"
  if [[ "${SHARE_DIRECTORY_REPLACE_DIRECTIVES}" == "true" ]]; then 
    if [[ "${SHARE_DIRECTORY_DIRECTIVES}" != "" ]]; then 
      printf "%s\n" "$SHARE_DIRECTORY_DIRECTIVES" | sed 's/^/    /g'
    fi
  else
    echo "    DirectoryIndex ${RANDOM}${RANDOM}${RANDOM}.IDontExist"
    echo "    AllowOverride none"
    echo "    Options +Indexes"
    echo "    Dav On"
    if [[ "${SHARE_DIRECTORY_DIRECTIVES}" != "" ]]; then 
      printf "%s\n" "$SHARE_DIRECTORY_DIRECTIVES" | sed 's/^/    /g'
    fi
    echo "    AuthType Basic"
    echo "    AuthName \"${SHARE_AUTHNAME}\""
    echo "    AuthUserFile \"${SHARE_PASSWORD_FILE}.gen\""
    echo "    Require valid-user"
  fi
  echo "  </Directory>"
  echo

  let x+=1
done

echo '</VirtualHost>'
