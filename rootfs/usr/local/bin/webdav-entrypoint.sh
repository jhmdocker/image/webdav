#!/bin/bash -e

# enable mods listed in APACHE_MODS
for x in ${APACHE_MODS}; do
  a2enmod $x
done

## Silence the default index.html.
echo "" > /var/www/html/index.html

/usr/local/bin/webdav-genconf.sh "${CONFIG_FILE}" > /tmp/dav.conf

# remove last couple of lines of the site config
sed -i 's/<\/VirtualHost>.*//g' /etc/apache2/sites-enabled/000-default.conf
sed -i 's/# vim\:.*//g' /etc/apache2/sites-enabled/000-default.conf

# append new config to 000-default.conf
cat /etc/apache2/sites-enabled/000-default.conf /tmp/dav.conf > /tmp/site.conf
cp -f /tmp/site.conf /etc/apache2/sites-enabled/000-default.conf
rm -fr /tmp/*.conf

echo
echo "######################################################################"
cat /etc/apache2/sites-enabled/000-default.conf
echo "######################################################################"
echo
exec /usr/local/bin/apache-entrypoint.sh "$@"
