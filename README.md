# Apache Webdav Server with Yaml Config

A containerized Apache installation with dav modules enabled and ready to be used. Configuration is done by simplified Yaml file.

## Running An Example

While simple, the runtime is tightly coupled with its configuration file. So, learning how to configure it is important to get a container running. The simplest configuration example can be found in `examples/simple.yaml`.

```yaml
shares:
- name: myshare
  users:
  - name: user
    password: notsosecret
```

With `simple.yaml`, the container will attempt to share an internal directory `/var/webdav/myshare`, giving it HTTP Basic Auth access to a single user; in this case username `user` with password `notsosecret`. It's important to note that **the internal shared directory is not created by the container**, so you will need to provide that at runtime.

> **IMPORTANT:** The contents of the storage volume must have read/write access to the Apache uid/gid used inside the container. For this default installation of Apache, that is UID `www-data(33)` and GID `www-data(33)`;

With this in mind, at least two elements will be needed: the configuration file and a volume mounted internally at `/var/webdav/<my_share_name>`.

Running this example should be as simple as:

```bash
docker run --rm -d --name webdav \
  -v $PWD/tmp/shareddir:/var/webdav/myshare \
  -v $PWD/examples/simple.yaml:/etc/webdav/config.yaml \
  registry.gitlab.com/jhmdocker/webdav:1.6
```

Mounting a volume is required because this container assumes you will be sharing content from a persistent storage. If you are just testing, you can always point the volume to anywhere disposable in your system.

Apache access logs are sent to `stdout` and error logs to `stderr`. Therefore, those can be viewed in the container logs.

```bash
docker logs -f webdav
```

You can check if the share is working by accessing it from your browser or any other DAV client.

```bash
http://<CONTAINER_IP>/myshare/
```

## How It Works

Simply put, the container will read the Yaml configuration and prepare apache's conf dynamically during its entrypoint. If you look at the container logs from the above `examples/simple.yaml`, you will notice the apache configuration is displayed during startup.

Given the information from `/etc/webdav/config.yaml`, the container edits `/etc/apache2/sites-enabled/000-default.conf`, and adds `Alias`, `<Directory>` entries, as well as all necessary directives to make the `myshare` available.

```apache
Alias /myshare /var/webdav/myshare
<Directory /var/webdav/myshare>
  DirectoryIndex 14736207467601.IDontExist
  AllowOverride none
  Options +Indexes
  Dav On
  AuthType Basic
  AuthName "myshare"
  AuthUserFile "/etc/webdav/myshare.htpasswd.gen"
  Require valid-user
</Directory>
```

## Configuration Reference And Examples

You will find a more complete set in the `examples` directories. Particularly, `examples/complete.yaml` aims to be a complete reference with comments to explain all options.

The `simple.yaml` example is very basic and only defines one shared directory. You can define as many shares as you want, adding more entries to the `.shares[]` property.

```yaml
shares:
- name: myshare1
  users:
  - name: user1
    password: notsosecret

- name: myshare2
  users:
  - name: user2
    password: notsosecret
```

Here, we have two independent shares and each one defines their own user list. You will find this example embedded into `examples/multiple-shares.yaml`. To run that example, use you will need to provide one volume for each shared directory.

```bash
docker run --rm -d --name webdav \
  -v $PWD/tmp/shareddir:/var/webdav/myshare1 \
  -v $PWD/tmp/shareddir:/var/webdav/myshare2 \
  -v $PWD/examples/simple.yaml:/etc/webdav/multiple-shares.yaml \
  registry.gitlab.com/jhmdocker/webdav:1.6
```

## Authorization Methods and Apache Modules

The container is prepared to use HTTP Basic Authenticiation. This Apache installation has several `auth_*` modules installed, but only `auth_basic` is enabled by default.

```text
root@7e61b78b1050:/etc/apache2/mods-available# ls auth*

auth_basic.load   authn_dbm.load      authz_dbd.load
auth_digest.load  authn_file.load     authz_dbm.load
auth_form.load    authn_socache.load  authz_groupfile.load
authn_anon.load   authnz_fcgi.load    authz_host.load
authn_core.load   authnz_ldap.load    authz_owner.load
authn_dbd.load    authz_core.load     authz_user.load
```

It is possible to enable any other module, by redefining the environment variable `APACHE_MODS` in the container runtime.

```bash
docker run --rm -d --name webdav \
  -e APACHE_MODS="auth_basic auth_digest authnz_ldap"
  -v $PWD/tmp/shareddir:/var/webdav/myshare \
  -v $PWD/examples/simple.yaml:/etc/webdav/config.yaml \
  registry.gitlab.com/jhmdocker/webdav:1.6
```

It is important to note that, currently, only `auth_basic` is supported by this container's Yaml configuration parser. It assumes Basic HTTP Auth for all apache configuration it creates. Simply enabling a new Apache module won't change this behavior.

If you need to use any other AuthType method, you will need to rewrite the directives for the share's `<Directory>` entirely. To accomplish that, you can use the properties `.shares[].directory.replace-directives` and `.shares[].directory.directives`.

```yaml
shares:
- name: myshare
  directory:
    replace-directives: true
    directives: |
      Dav On
      AuthType: digest
      ## any other directive necessary
```

> **IMPORTANT:** In its default behavior, the container will include a few standard directives, like `Options`, `DirectoryIndex`, and `AllowOverride`. If you use `replace-directives: true`, the `<Directory>` entry for a given share will be empty and you will need to include all the directives you want/need from scratch.

## Customizing The \<Directory\> Configuration

By default, the container will create a `<Directory>` entry for every share you define. You can see from the output of running `examples/simple.yaml` what the default directives are:

```apache
Alias /myshare /var/webdav/myshare
<Directory /var/webdav/myshare>
  DirectoryIndex 14736207467601.IDontExist
  AllowOverride none
  Options +Indexes
  Dav On
  AuthType Basic
  AuthName "myshare"
  AuthUserFile "/etc/webdav/myshare.htpasswd.gen"
  Require valid-user
</Directory>
```

> **NOTE:** `DirectoryIndex` gets a random value, pointing to a non-existing file. This index name will change on every restart of the container. The reason for this random value is to avoid problems with certain WebDAV clients that are unable to list the contents of the share if a file called `index.html` is already present. This is particularly common if your shared contents make up a website with HTML resources.

If you need to include more directives, you can use the `.shares[].directory.directives` property.

```yaml
shares:
- name: myshare
  directory:
    directives: |
      DavDepthInfinity On
      DavMinTimeout 600
      LimitXMLRequestBody 524288000
```

This configuration appends three extra directives, allowing you to further customize the behavior the `<Directory>` entry of your share. Any valid Apache directive can be used here.

> **IMPORTANT:** If you are just appending extra directives, make sure the `.shares[].directory.replace-directives` is `false`. Otherwise, the entire contents of the `<Directory>` configuration will be cleared and replaced with the contents of `.shares[].directory.directives`.

You will find an example of this type of customization in `examples/extra-directives.yaml`.

## Users and Passwords

As seen up to now, you can easily define users directly in the Yaml config file. In most examples, `password` is used in clear plain text, which is not always desired for obvious security reasons.

You also have the option to define `password-hash` instead of a clear text. Password hashes can be created using the standard Apache tool `htpasswd`.

```text
root@7e61b78b1050:/# htpasswd -nBb user secretpass
user:$2y$05$CXwj0f.dsVGPO4OL7I2A8.IIrQLUtxwMVOElEqp2pOu7mLNPIaSMu
```

Because this tool is used by the container to create hashes from clear text passwords, `/usr/bin/htpasswd` is already included in the image.

### Providing Your Own htpasswd file

When the container starts, user configuration is parsed and a temporary custom `htpasswd` file is created for each share.

It is possible to provide your own pre-existing password file by simply mounting as a volume to the container. For the filename inside the container, use the same share name you defined in your Yaml config.

Example: `/etc/webdav/<share_name>.htpasswd`

```bash
docker run --rm -d --name webdav \
  -v $PWD/tmp/shareddir:/var/webdav/myshare \
  -v $PWD/examples/simple.yaml:/etc/webdav/config.yaml \
  -v $PWD/examples/users.htpasswd:/etc/webdav/myshare.htpasswd \
  registry.gitlab.com/jhmdocker/webdav:1.6
```

The example `examples/users.htpasswd` includes two pre-existing users, `jon` and `mary`. You might be wondering what happens here, since `examples/simple.yaml` already defines another one called `user`. In this case, no information is ever lost, and users defined in both places will be used. No modifications will made to your file outside the container. The complete user list is merged into a temporary file to be used at runtime.

To resolve conflicts, the Yaml config takes precendence. If a username appears in both places, only the one from Yaml config will be used.

### Global Users For All Shares

While each share can have its own user list, you can also define a global list of users.

```yaml
users:
- name: admin
  password: notsosecret
- name: manager
  password: notsosecret

shares:
- name: myshare1
  users:
  - name: jon
    password: notsosecret
- name: myshare2
  users:
  - name: mary
    password: notsosecret
```

In this case, the users `admin` and `manager` will be appended along with each share's user list.

The final password file used for `myshare1` will be similar to

```text
admin:$2y$05$d3pC6qaNrRpcENHRRYFL8eF8jhha.YWjLctkAdqIOB7sbKawlwh7C
manager:$2y$05$JeYZF4P62Lzo373TpoDn/uhJ6UfAEIh2VmvVJY3WOPSVXeN2fD/jm
jon:$2y$05$FIWB5bSyteHhiK6BqvTsNe5RdeGAhToNOAscXjKr2wfNr27f66jTq
```

... while for `myshare2`, it'll be

```text
admin:$2y$05$UIcg0P7RQjKUyoKIg/vpvOxFDQIVAHyqlZdWmnBrhcUINKppyiHnO
manager:$2y$05$BtVgjDaIbT.Ej8wrQV124OAs15q89UuTNotWWqpZRSGnwaMp9Zu0a
mary:$2y$05$95LM4aKnGeFRU.W0i0YbD.W.9e889JrGcafbtnLEIREhAn2dbxJQy
```

> **NOTE:** You may notice that the password hashes for `admin` and `manager` do not match among the config used for `myshare1` and `myshare2`. This happens because, for each share, a new set of htpasswd hashes is created. The hash value will only match if you youse `password-hash` instead of the plain text `password` property.
